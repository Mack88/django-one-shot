from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method =="POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect ("todo_list_detail", id=form.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
        }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "task_list": post,
        "form":form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    list_to_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_to_delete.delete()
        return redirect(todo_list)

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)
