# Generated by Django 5.0 on 2023-12-13 19:04

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="list",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="item",
                to="todos.todolist",
            ),
        ),
    ]
